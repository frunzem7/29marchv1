import java.util.Calendar;
import java.util.Locale;
import java.util.Scanner;

public class Student {
    public static void main(String[] args) {
        System.out.println("Intrare in program...");

        Scanner sc = new Scanner(System.in);
        System.out.println();

        String grupa = null;

        System.out.print("Introduce facultatea(Matematica/Informatica): ".toUpperCase());
        String facultate = sc.next();


        // Add your code here
        if (facultate.equals("Matematica")) {
            System.out.println("A fost aleasa Facultatea " + facultate);
        } else if (facultate.equals("Informatica")) {
            System.out.println("A fost aleasa Facultatea " + facultate);
        } else {
            System.exit(0);
        }

        System.out.print("Doriti sa introduceti studenti noi? (Da/Nu)");
        String choice = sc.next();
        if(choice.equals("Da")) {
            System.out.print("Creati numele grupei noi:  ");
            grupa = sc.next();
        } else if (choice.equals("da")) {
            System.out.print("Creati numele grupei noi:  ");
            grupa = sc.next();
        } else if (choice.equals("Nu")) {
            System.exit(0);
        }  else if (choice.equals("nu")) {
            System.exit(0);
        }

        System.out.println("Introducerea studentilor grupei " + grupa);
        System.out.print("Introduceti numele studentului 1: ");
        String name = sc.next();
        System.out.print("Introduceti anul nasterii: ");
        int year = sc.nextInt();
        System.out.println("Student: " + name + ", Varsta: " + (Calendar. getInstance(). get(Calendar. YEAR)-year));

        System.out.print("Introduceti Specialitatea (Algebra/Geometrie/Calcul_Integral): ");
        String specialitatea2 = sc.next() ;
        if(specialitatea2.equals("Algebra")||specialitatea2.equals("Geometrie")||specialitatea2.equals("Calcul_Integral")) {
            System.out.print("Introduceti nota test1: ");
            int notaTest1 = sc.nextInt();
            System.out.print("Introduceti nota test2: ");
            int notaTest2 = sc.nextInt();
            System.out.print("Introduceti nota examen: ");
            int notaExamen = sc.nextInt();

            //hadjioglo code
            //calculare medie
            double medie = 0;
            medie = (notaTest1 + notaTest2 + notaExamen ) /3.0;

            //check if Nota examen is more then 9
            if (medie > 9){
                System.out.println();
                System.out.println("Esti Promovat!");
            }else System.out.println("Nu ati trecut cu brio, incercati in semestrul urmator");
            System.out.println();
            //hadjioglo code ends here

        }else {
            System.exit(0);
        }
        System.out.println("Iesire din program...");
    }
}
